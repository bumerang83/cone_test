
package cone_test_andrei_lunin_13_06_2017;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bumerang
 */
public class BoxTest {
    
    public BoxTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of toString method, of class Box.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
//        Box instance = new Box();
        // creates and fills an array
        Box[] box = new Box[]{new Box(),new Box(),new Box(),new Box(),new Box(),new Box(),new Box(),new Box(),new Box(),new Box(),new Box(),new Box()};
        String expResult = "";
//        String result = instance.toString();
        // checks the strings
        for (Box boxTest : box) {
            String result = boxTest.toString();  
            assertNotEquals(expResult, result);
        }
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }
}
