/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cone_test_andrei_lunin_13_06_2017;

import java.io.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bumerang
 */
public class CheckingsTest {
    
    public CheckingsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of checkEqualTen method, of class Checkings.
     */
    @Test
    public void testCheckEqualTen() {
        System.out.println("checkEqualTen");
//        Box[] box = null;
        // reads input from file
        ReadInputFile.dataInput(new File("input.txt"));  // after calling this method, the array was filled with the values from the input file
        // creates the array of Boxes
        Box box[] = ReadInputFile.box;
//        boolean expResult = false;
        boolean expResult = true;
        // calls the been tested method
        boolean result = Checkings.checkEqualTen(box);
        // checkes if result was successful
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of lessThanTen method, of class Checkings.
     */
    @Test
    public void testLessThanTen() {
        System.out.println("lessThanTen");
//        Box[] box = null;
        // reads input from file
        ReadInputFile.dataInput(new File("input.txt"));  // after calling this method, the array was filled with the values from the input file
        // creates the array of Boxes
        Box box[] = ReadInputFile.box;
//        boolean expResult = false;
        boolean expResult = true;
        boolean result = Checkings.lessThanTen(box);
        // checkes if result was successful
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }
    
}
