
package cone_test_andrei_lunin_13_06_2017;

/**
 * project name: Cone_Test_Andrei_Lunin_13_06_2017
 * package name: cone_test_andrei_lunin_13_06_2017
 * file name: Checkings.java
 * project version: default
 * Created:
 * Jun 13, 2017, 10:49:58 AM
 * @author bumerang
 */
// class for checking different combinations
public class Checkings {

    // checking for required sets are equal 10
    public static boolean checkEqualTen(Box[] box) {
        if(box[0].idx3 + box[1].idx2 + box[3].idx1 + box[4].idx0 != 10) {
            return false;  // the combination is not good
        }
        if(box[2].idx3 + box[3].idx2 + box[6].idx1 + box[7].idx0 != 10) {
            return false;  // the combination is not good
        }
        if(box[3].idx3 + box[4].idx2 + box[7].idx1 + box[8].idx0 != 10) {
            return false;  // the combination is not good
        }
        if(box[4].idx3 + box[5].idx2 + box[8].idx1 + box[9].idx0 != 10) {
            return false;  // the combination is not good
        }
        if(box[7].idx3 + box[8].idx2 + box[10].idx1 + box[11].idx0 != 10) {
            return false;  // the combination is not good
        }
        
        return true;  // the combination is good in other case if it was not returned earlier
    }
    
    // checking for required sets are less than 10
    public static boolean lessThanTen(Box[] box) {
        if(box[0].idx1 + box[1].idx0 > 10) {
            return false;
        }
        if(box[0].idx2 + box[2].idx1 + box[3].idx0 > 10) {
            return false;
        }
        if(box[1].idx3 + box[4].idx1 + box[5].idx0 > 10) {
            return false;
        }
        if(box[2].idx2 + box[6].idx0 > 10) {
            return false;
        }
        if(box[5].idx3 + box[9].idx1 > 10) {
            return false;
        }
        if(box[6].idx3 + box[7].idx2 + box[10].idx0 > 10) {
            return false;
        }
        if(box[8].idx3 + box[9].idx2 + box[11].idx1 > 10) {
            return false;
        }
        if(box[10].idx3 + box[11].idx2 > 10) {
            return false;
        }
        return true;  // if everything is good
    }
}
