
package cone_test_andrei_lunin_13_06_2017;

import java.io.*;
import org.paukov.combinatorics.Factory;
import org.paukov.combinatorics.Generator;
import org.paukov.combinatorics.ICombinatoricsVector;

/**
 *
 * project name: Cone_Test_Andrei_Lunin_13_06_2017
 * package name: cone_test_andrei_lunin_13_06_2017
 * file name: Cone_Test_Andrei_Lunin_13_06_2017
 * project version: default
 * Created:
 * Jun 13, 2017, 10:24:36 AM
 * @author bumerang
 */
public class Cone_Test_Andrei_Lunin_13_06_2017 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ReadInputFile.dataInput(new File("input.txt"));  // after calling this method, the array was filled with the values from the input file
        // a new array of 'Box' for action
        Box box[] = new Box[12];
        
        ICombinatoricsVector<Integer> boxVector = Factory
                .createVector(new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11});
        Generator<Integer> boxGen = Factory
                .createPermutationGenerator(boxVector);
        
        // enables all combinations
        for (ICombinatoricsVector<Integer> perm : boxGen) {
            // gets the current combination to check it
            for(int i = 0; i < 12; i++) {
                box[i] = ReadInputFile.box[perm.getValue(i)];
            }
            // checks the combination
            if(Checkings.checkEqualTen(box)) {  // five required 'tens'
                if(Checkings.lessThanTen(box)) {  // eight required 'not more than tens'
                    // if the combination succeeded checkings
                    System.out.println("BINGO! ->");
                    for(int j = 0; j < box.length; j++) {
//                        System.out.println((j+1) + ") " + box[j]);
                        System.out.println(box[j]);  // prints the result on console
                    }
                    System.out.println();
                }
            }
        }
    }
}