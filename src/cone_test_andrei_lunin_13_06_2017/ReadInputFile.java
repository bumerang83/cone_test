
package cone_test_andrei_lunin_13_06_2017;

import java.io.*;

/**
 * project name: Cone_Test_Andrei_Lunin_13_06_2017
 * package name: cone_test_andrei_lunin_13_06_2017
 * file name: ReadInputFile.java
 * project version: default
 * Created:
 * Jun 13, 2017, 10:28:49 AM
 * @author bumerang
 */
// class for reading input data
public class ReadInputFile {
        static Box[] box;  // an array of Boxes, which we initialize and fill using the input file
		
	// a static initialization block
	static {
            box = new Box[12]; 
	}

    	public static void dataInput(File input) {  // the input method, reading data from the input file
            try {
                FileReader fr = new FileReader(input);  // getting an object for reading data from the input file
                BufferedReader br = new BufferedReader(fr);  // getting a stream for reading data by Strings from the input file
                String str;  // for reading data from the input file line-by-line
                //Box[] box = new Box[12];  // this array is the main object in the program, I would even say the main input store

                // reading from the input file
                for(int i = 0; i < 12; i++) { 
                    str = br.readLine();  // reads a new String
                    box[i] = new Box(str.charAt(0), str.charAt(1), str.charAt(2), str.charAt(3));  // creates a new Box object
//                  System.out.println("old index " + i + "\t" + box[i]);  // printing out some testing information
                }
                br.close();  // immediately closing the input stream in the end
            } catch (IOException e) {  // if some other exception occurred while opening/reading the input file
                // TODO Auto-generated catch block
                System.out.println("Error reading data from file " + input.getName());
                e.printStackTrace();
            }
	}
}