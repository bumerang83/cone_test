/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cone_test_andrei_lunin_13_06_2017;

/**
 * project name: Cone_Test_Andrei_Lunin_13_06_2017
 * package name: cone_test_andrei_lunin_13_06_2017
 * file name: Box.java
 * project version: default
 * Created:
 * Jun 13, 2017, 10:32:12 AM
 * @author bumerang
 */
// class for operations with the input data
public class Box {
	int idx0;  // four integer numbers in a box
	int idx1;
	int idx2;
	int idx3;
	
	Box() {  // a constructor - randomly initializing, testings
		idx0 = (int)Math.round(Math.random() * 9);  // filling randomly from 0 to 9 each field
		idx1 = (int)Math.round(Math.random() * 9);
		idx2 = (int)Math.round(Math.random() * 9);
		idx3 = (int)Math.round(Math.random() * 9);
	}
	
	// a constructor for testings
	Box(int idx0, int idx1, int idx2, int idx3) {
		this.idx0 = idx0;
		this.idx1 = idx1;
		this.idx2 = idx2;
		this.idx3 = idx3;
	}
	
	// the main constructor, for creating an array of the Box objects
	Box(char idx0, char idx1, char idx2, char idx3) {
		this.idx0 =  Character.getNumericValue(idx0);
		//System.out.println("" + this.idx0);
		this.idx1 = Character.getNumericValue(idx1);
		//System.out.println("" + this.idx1);
		this.idx2 = Character.getNumericValue(idx2);
		//System.out.println("" + this.idx2);
		this.idx3 = Character.getNumericValue(idx3);
		//System.out.println("" + this.idx3);
	}
	
        @Override
	public String toString() {  // overloading toString method of the Object class
		return "" + idx0 + " " + idx1 + " " + idx2 + " " + idx3;
	}
}